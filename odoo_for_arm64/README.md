First, for an arm64 architecture, you need to create the docker image of odoo. 
The dockerfile is available in the DockerFile folder. 
Run the following commands to create the image:

`cd /docker/odoo_for_arm64/DockerFile/`


`docker build -t odoo:arm64_v15 .`


